from django.db import models


# Create your models here.
class Business(models.Model):
    uuid = models.CharField(max_length=200, null=True)
    name = models.CharField(max_length=200, null=True)
    legalIdentifier = models.CharField(max_length=200, null=True)
    status = models.CharField(max_length=200, null=True)
    created = models.TimeField()
    modified = models.TimeField()


class Customer(models.Model):
    businessUUID = models.CharField(max_length=200, null=True)
    uuid = models.CharField(max_length=200, null=True)
    firstName = models.CharField(max_length=200, null=True)
    lastName = models.CharField(max_length=200, null=True)
    email = models.CharField(max_length=200, null=True)
    mobileNumber = models.CharField(max_length=200, null=True)
    title = models.CharField(max_length=200, null=True)
    created = models.TimeField()
    modified = models.TimeField()


class Contact(models.Model):
    businessUUID = models.CharField(max_length=200, null=True)
    uuid = models.CharField(max_length=200, null=True)
    firstName = models.CharField(max_length=200, null=True)
    lastName = models.CharField(max_length=200, null=True)
    email = models.CharField(max_length=200, null=True)
    mobileNumber = models.CharField(max_length=200, null=True)
    title = models.CharField(max_length=200, null=True)
    created = models.TimeField()
    modified = models.TimeField()


class Lead(models.Model):
    uuid = models.CharField(max_length=200, null=True)
    businessUUID = models.CharField(max_length=200, null=True)
    company = models.CharField(max_length=200, null=True)
    firstName = models.CharField(max_length=200, null=True)
    lastName = models.CharField(max_length=200, null=True)
    email = models.CharField(max_length=200, null=True)
    phoneNumber = models.CharField(max_length=200, null=True)
    status = models.CharField(max_length=200, null=True)
    created = models.TimeField()
    modified = models.TimeField()


class Project(models.Model):
    uuid = models.CharField(max_length=200, null=True)
    businessUUID = models.CharField(max_length=200, null=True)
    customerUUID = models.CharField(max_length=200, null=True)
    name = models.CharField(max_length=200, null=True)
    status = models.CharField(max_length=200, null=True)
    created = models.TimeField()
    modified = models.TimeField()


class ProjectTask(models.Model):
    uuid = models.CharField(max_length=200, null=True)
    projectUUID = models.CharField(max_length=200, null=True)
    note = models.CharField(max_length=200, null=True)
    internalNote = models.CharField(max_length=200, null=True)
    status = models.CharField(max_length=200, null=True)
    created = models.TimeField()
    modified = models.TimeField()


class Ticket(models.Model):
    uuid = models.CharField(max_length=200, null=True)
    businessUUID = models.CharField(max_length=200, null=True)
    customerUUID = models.CharField(max_length=200, null=True)
    projectUUID = models.CharField(max_length=200, null=True)
    name = models.CharField(max_length=200, null=True)
    assignedTo = models.CharField(max_length=200, null=True)
    requester = models.CharField(max_length=200, null=True)
    status = models.CharField(max_length=200, null=True)
    created = models.TimeField()
    modified = models.TimeField()


class TicketNote(models.Model):
    uuid = models.CharField(max_length=200, null=True)
    postedBy = models.CharField(max_length=200, null=True)
    businessUUID = models.CharField(max_length=200, null=True)
    customerUUID = models.CharField(max_length=200, null=True)
    projectUUID = models.CharField(max_length=200, null=True)
    note = models.CharField(max_length=200, null=True)
    internalNote = models.CharField(max_length=200, null=True)
    status = models.CharField(max_length=200, null=True)
    created = models.TimeField()
    modified = models.TimeField()
