from django.contrib import admin
from django.urls import path, include
from core import views

urlpatterns = [
    path("", views.Dashboard, name="Dashboard"),

    # Business
    path("businesses/", views.BusinessList, name="BusinessList"),
    path("businesses/add", views.BusinessAdd, name="BusinessAdd"),
    path("businesses/edit/<businessUUID>", views.BusinessEdit, name="BusinessEdit"),
    path("businesses/details/<businessUUID>", views.BusinessDetails, name="BusinessDetails"),
    path("businesses/delete/<businessUUID>", views.BusinessDelete, name="BusinessDelete"),

    # Customers
    path("customers/", views.CustomerList, name="CustomerList"),
    path("customers/add", views.CustomerAdd, name="CustomerAdd"),
    path("customers/add/<businessUUID>", views.CustomerAddToBusiness, name="CustomerAddToBusiness"),
    path("customers/edit/<customerUUID>", views.CustomerEdit, name="CustomerEdit"),
    path("customers/details/<customerUUID>", views.CustomerDetails, name="CustomerDetails"),
    path("customers/delete/<customerUUID>", views.CustomerDelete, name="CustomerDelete"),

    # Contacts
    path("contacts/", views.ContactList, name="ContactList"),
    path("contacts/add/<businessUUID>", views.ContactAdd, name="ContactAdd"),
    path("contacts/edit/<contactUUID>", views.ContactEdit, name="ContactEdit"),
    path("contacts/delete/<contactUUID>", views.ContactDelete, name="ContactDelete"),
]

