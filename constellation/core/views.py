import uuid
from datetime import datetime
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required


from core.models import Customer, Contact
from core.models import Business
from core.models import Project, ProjectTask
from core.models import Ticket, TicketNote


# Create your views here.
@login_required(login_url='/accounts/login/')
def Dashboard(request):
    context = {
        'user': request.user
    }

    return render(request, 'dashboard/index.html', context)


# Business Views
@login_required(login_url='/accounts/login/')
def BusinessList(request):
    businesses = Business.objects.all()
    context = {
        'businesses': businesses,
        'user': request.user
    }

    return render(request, 'business/list.html', context)

@login_required(login_url='/accounts/login/')
def BusinessAdd(request):
    if request.method == 'POST':
        newBusiness = Business(
            uuid=uuid.uuid4(),
            name=request.POST.get('name'),
            legalIdentifier=request.POST.get('legalIdentifier'),
            status="active",
            created=datetime.today(),
            modified=datetime.today()
        )

        newBusiness.save()
        return redirect('BusinessList')

    context = {
        'user': request.user
    }
    return render(request, 'business/form.html', context)

@login_required(login_url='/accounts/login/')
def BusinessEdit(request, businessUUID):
    business = Business.objects.get(uuid=businessUUID)

    if request.method == 'POST':
        business.name = request.POST.get('name')
        business.address = request.POST.get('legalIdentifier')
        business.status = request.POST.get('status')
        business.modified = datetime.today()

        business.save()
        return redirect('BusinessList')

    context = {
        'businessUUID': businessUUID,
        'business': business,
        'editForm': True,
        'user': request.user
    }

    return render(request, 'business/form.html', context)

@login_required(login_url='/accounts/login/')
def BusinessDetails(request, businessUUID):
    business = Business.objects.get(uuid=businessUUID)
    contacts = Contact.objects.filter(businessUUID=businessUUID)
    customers = Customer.objects.filter(businessUUID=businessUUID)

    context = {
        'businessUUID': businessUUID,
        'business': business,
        'contacts': contacts,
        'customers': customers,
        'user': request.user
    }

    return render(request, 'business/details.html', context)

@login_required(login_url='/accounts/login/')
def BusinessDelete(request, businessUUID):
    Business.objects.get(uuid=businessUUID).delete()
    return redirect('BusinessList')


# Customer Views
@login_required(login_url='/accounts/login/')
def CustomerList(request):
    customers = Customer.objects.all()
    context = {
        'customers': customers,
        'user': request.user
    }

    return render(request, 'customer/list.html', context)

@login_required(login_url='/accounts/login/')
def CustomerAddToBusiness(request, businessUUID):
    if request.method == 'POST':
        newCustomer = Customer(
            uuid=uuid.uuid4(),
            businessUUID=businessUUID,
            firstName=request.POST.get('firstName'),
            lastName=request.POST.get('lastName'),
            email=request.POST.get('email'),
            mobileNumber=request.POST.get('mobileNumber'),
            title=request.POST.get('title'),
            created=datetime.today(),
            modified=datetime.today()
        )

        newCustomer.save()
        return redirect('BusinessDetails', businessUUID=businessUUID)

    context = {
        'businessLink': True,
        'businessUUID': businessUUID,
        'user': request.user
    }
    return render(request, 'customer/form.html', context)

@login_required(login_url='/accounts/login/')
def CustomerAdd(request):
    businesses = Business.objects.all()

    if request.method == 'POST':
        newCustomer = Customer(
            uuid=uuid.uuid4(),
            businessUUID=request.POST.get('businessUUID'),
            firstName=request.POST.get('firstName'),
            lastName=request.POST.get('lastName'),
            email=request.POST.get('email'),
            mobileNumber=request.POST.get('mobileNumber'),
            title=request.POST.get('title'),
            created=datetime.today(),
            modified=datetime.today()
        )

        newCustomer.save()
        return redirect('CustomerList')

    context = {
        'businesses': businesses,
        'user': request.user
    }
    return render(request, 'customer/form.html', context)

@login_required(login_url='/accounts/login/')
def CustomerEdit(request, customerUUID):
    customer = Customer.objects.get(uuid=customerUUID)

    if request.method == 'POST':
        customer.name = request.POST.get('name')
        customer.address = request.POST.get('address')
        customer.phoneNumber = request.POST.get('phoneNumber')
        customer.modified = datetime.today()

        customer.save()
        return redirect('CustomerList')

    context = {
        'customerUUID': customerUUID,
        'customer': customer,
        'editForm': True,
        'user': request.user
    }

    return render(request, 'customer/form.html', context)

@login_required(login_url='/accounts/login/')
def CustomerDetails(request, customerUUID):
    customer = Customer.objects.get(uuid=customerUUID)
    contacts = Contact.objects.filter(customerUUID=customerUUID)

    context = {
        'customerUUID': customerUUID,
        'contacts': contacts,
        'customer': customer,
        'user': request.user
    }

    return render(request, 'customer/details.html', context)

@login_required(login_url='/accounts/login/')
def CustomerDelete(request, customerId):
    Customer.objects.get(id=customerId).delete()
    return redirect('CustomerList')


# Contact Views
@login_required(login_url='/accounts/login/')
def ContactList(request):
    contacts = Contact.objects.all()
    context = {
        'contacts': contacts,
        'user': request.user
    }

    return render(request, 'contacts/list.html', context)

@login_required(login_url='/accounts/login/')
def ContactAdd(request, businessUUID):
    if request.method == 'POST':
        newContact = Contact(
            uuid=uuid.uuid4(),
            businessUUID=businessUUID,
            firstName=request.POST.get('firstName'),
            lastName=request.POST.get('lastName'),
            email=request.POST.get('email'),
            mobileNumber=request.POST.get('mobileNumber'),
            title=request.POST.get('title'),
            created=datetime.today(),
            modified=datetime.today()
        )

        newContact.save()
        return redirect('BusinessDetails', businessUUID=businessUUID)

    context = {
        'businessUUID': businessUUID,
        'user': request.user
    }
    return render(request, 'contact/form.html', context)

@login_required(login_url='/accounts/login/')
def ContactEdit(request, contactUUID):
    contact = Contact.objects.get(uuid=contactUUID)

    if request.method == 'POST':
        contact.firstName = request.POST.get('firstName')
        contact.lastName = request.POST.get('lastName')
        contact.email = request.POST.get('email')
        contact.mobileNumber = request.POST.get('mobileNumber')
        contact.title = request.POST.get('title')
        contact.modified = datetime.today()

        contact.save()
        return redirect('BusinessDetails', businessUUID=contact.businessUUID)

    context = {
        'contactUUID': contactUUID,
        'contact': contact,
        'editForm': True,
        'user': request.user
    }

    return render(request, 'contact/form.html', context)

@login_required(login_url='/accounts/login/')
def ContactDelete(request, contactUUID):
    contact = Contact.objects.get(uuid=contactUUID)
    Contact.objects.get(uuid=contactUUID).delete()
    return redirect('BusinessDetails', businessUUID=contact.businessUUID)

# Project Views
@login_required(login_url='/accounts/login/')
def ProjectList(request):
    projects = Project.objects.all()
    context = {
        'projects': projects,
        'user': request.user
    }

    return render(request, 'project/list.html', context)

@login_required(login_url='/accounts/login/')
def ProjectAdd(request):
    if request.method == 'POST':
        newProject = Project(
            uuid=uuid.uuid4(),
            name=request.POST.get('name'),
            status="new",
            created=datetime.today(),
            modified=datetime.today()
        )

        newProject.save()
        return redirect('ProjectList')

    context = {
        'user': request.user
    }
    return render(request, 'project/form.html', context)

@login_required(login_url='/accounts/login/')
def ProjectEdit(request, projectUUID):
    project = Project.objects.get(uuid=projectUUID)

    if request.method == 'POST':
        project.businessUUID = request.POST.get('businessUUID')
        project.customerUUID = request.POST.get('customerUUID')
        project.name = request.POST.get('name')
        project.modified = datetime.today()

        project.save()
        return redirect('ProjectDetails', projectUUID=project.uuid)

    context = {
        'projectUUID': projectUUID,
        'project': project,
        'editForm': True,
        'user': request.user
    }

    return render(request, 'project/form.html', context)

@login_required(login_url='/accounts/login/')
def ProjectDelete(request, projectUUID):
    project = Project.objects.get(uuid=projectUUID)
    Project.objects.get(uuid=projectUUID).delete()
    return redirect('ProjectDetails', projectUUID=project.uuid)

